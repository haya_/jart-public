# SRB2Kart (jart fork)

[SRB2Kart](https://srb2.org/mods/) is a kart racing mod based on the 3D Sonic the Hedgehog fangame [Sonic Robo Blast 2](https://srb2.org/), based on a modified version of [Doom Legacy](http://doomlegacy.sourceforge.net/).

This is a fork of SRB2Kart that may or may not act as the "Moe's Mansion" of 1.5-master.

# Features
- Birdhouse included
    - Miscellaneous fixes
    - map-by-name
    - Custom resolutions
- Birdmod included
    - View rolling and tilting
    - Music fading
    - Music sync
- Snowy-Kart included
    - Additional HUD changes
- Other stuff ive added
    - Player sprite and slope rolling
    - Player x and y scaling
    - Powerup music toggle

## Dependencies
- NASM (x86 builds only)
- SDL2 (Linux/OS X only)
- SDL2-Mixer (Linux/OS X only)
- libupnp (Linux/OS X only)
- libgme (Linux/OS X only)

## Compiling

See [SRB2 Wiki/Source code compiling](http://wiki.srb2.org/wiki/Source_code_compiling). The compiling process for SRB2Kart is largely identical to SRB2.

## Requirements to run
- SRB2Kart asset files.
- [extra.kart](https://cdn.discordapp.com/attachments/845903070222614561/1023437829352718437/extra.kart)

## Disclaimer
Kart Krew is in no way affiliated with SEGA or Sonic Team. We do not claim ownership of any of SEGA's intellectual property used in SRB2.
